#!/bin/sh
src=`pwd`
tmp=$src/tests-tmp
rm -rf $tmp
mkdir -p $tmp
PATH="$src:/bin:/usr/bin:/usr/local/bin"
tests_failed=0
tests_count=0
_UID=`id -u`
_GID=`id -g`

usage() {
  echo "usage: sh $0 [-v]"
}

vecho() { :; }
while getopts v flag
do
  case $flag in
    v)      vecho() { echo "$*"; } ;;
	*)      usage; exit 1 ;;
  esac
done
export USER=`whoami`
export HOST=`hostname -f`
export SHOST=`hostname -s`
export BCRON_SOCKET=$tmp/spool-sock
export BCRON_SPOOL=$tmp/spool
export BCRON_USER=$USER
export BCRONTAB_SOCKET=$tmp/socket
export CRONTABS=$BCRON_SPOOL/crontabs

mkdir $BCRON_SPOOL
mkdir $CRONTABS
mkdir $BCRON_SPOOL/tmp
mkfifo $BCRON_SPOOL/trigger

netstring() {
    len=$( printf "$@" | wc -c )
    fmt="$1"
    shift
    printf "%d:${fmt}," $len "$@"
}

spoolsockcmd() {
  unixclient $BCRONTAB_SOCKET sh -c "
    perl -e '
      \$string = join(\"\", <>);
      printf \"%d:%s,\", length(\$string), \$string;
    ' >&7; cat <&6; echo"
}

spoolcmd() {
    perl -e '
      $string = join("", <>);
      printf "%d:%s,", length($string), $string;
    ' | env PROTO=UNIX UNIXREMOTEEUID=`id -u` $src/bcron-spool "$@" 2>/dev/null
    echo
}

sched() {
  MSG_DEBUG=0 $src/test-sched "$@" 2>&1
}

startsched() {
    sched_pid=$( $src/bcron-sched >$tmp/sched-log 2>&1 & echo $! )
    sleep 1
}

stopsched() {
    sleep 1
    kill $sched_pid

    sed -e '
        s/next:....-..-.. ..:..:.. [^ ]*/next:####-##-## ##:##:## #TZ/;
    ' <$tmp/sched-log
}

startspool() {
    spool_pid=$( unixserver $BCRON_SOCKET $src/bcron-spool > $tmp/spool-log 2>&1 & echo $! )
    sleep 1
}

stopspool() {
    kill $spool_pid
    cat $tmp/spool-log | sed -e 's/\[[0-9]*\]:/[#]:/'
}

catsub() {
    cat -v "$@" \
    | sed -e "
        s/($USER)/(USER)/g
        s/<$USER>/<USER>/g
        s/<$USER@/<USER@/g
	s/@$HOST>/@HOST>/g
	s/@$SHOST>/@HOST>/g
    "
}

doexec() {
    num=1
    for cmd in "$@"
    do
	netstring "${num}\\0${USER}\\0${cmd}"
	num=$(($num+1))
    done | MSG_DEBUG=0 TESTMODE=1 $src/bcron-exec >$tmp/exec.out 2>$tmp/exec.err
    catsub $tmp/exec.out; echo
    catsub $tmp/exec.err
}

run_compare_test() {
  local name=$1
  shift
  sed -e "s:@SOURCE@:$src:g"   	-e "s:@TMPDIR@:$tmp:g"   	-e "s:@UID@:$_UID:" 	-e "s:@GID@:$_GID:" 	>$tmp/expected
  ( runtest "$@" 2>&1 ) 2>&1 >$tmp/actual-raw
  cat -v $tmp/actual-raw >$tmp/actual
  if ! cmp $tmp/expected $tmp/actual >/dev/null 2>&1
  then
    echo "Test $name $* failed:"
	( cd $tmp; diff -U 9999 expected actual | tail -n +3; echo; )
	tests_failed=$(($tests_failed+1))
  fi
  rm -f $tmp/expected $tmp/actual
  tests_count=$(($tests_count+1))
}

##### Test tests/spool-list-gooduser #####

runtest() {
echo crontab > $CRONTABS/$USER
printf "L$USER" | spoolcmd
rm -f $CRONTABS/$USER
}
vecho "Running test tests/spool-list-gooduser "
run_compare_test tests/spool-list-gooduser  <<END_OF_TEST_RESULTS
9:Kcrontab
,
END_OF_TEST_RESULTS


##### Test tests/spool-list-perms #####

runtest() {
touch $CRONTABS/$USER
chmod 000 $CRONTABS/$USER
printf "L$USER" | spoolcmd
rm -f $CRONTABS/$USER
}
vecho "Running test tests/spool-list-perms "
run_compare_test tests/spool-list-perms  <<END_OF_TEST_RESULTS
23:ZCould not read crontab,
END_OF_TEST_RESULTS


##### Test tests/spool-delete-nouser #####

runtest() {
printf "Rxyzzy$USER" | spoolcmd
}
vecho "Running test tests/spool-delete-nouser "
run_compare_test tests/spool-delete-nouser  <<END_OF_TEST_RESULTS
28:DInvalid or unknown username,
END_OF_TEST_RESULTS


##### Test tests/sched-dst #####

runtest() {
local job="$1"
local start="$2"
export TZ=EST5EDT
sched "$job echo" $start
unset TZ
}
vecho "Running test tests/sched-dst '0 * * * *' '1081054860'"
run_compare_test tests/sched-dst '0 * * * *' '1081054860' <<END_OF_TEST_RESULTS
last: 1081054860 2004-04-04 00:01:00 EST
next: 1081058400 2004-04-04 01:00:00 EST
END_OF_TEST_RESULTS

vecho "Running test tests/sched-dst '0 * * * *' '1081058460'"
run_compare_test tests/sched-dst '0 * * * *' '1081058460' <<END_OF_TEST_RESULTS
last: 1081058460 2004-04-04 01:01:00 EST
next: 1081062000 2004-04-04 03:00:00 EDT
END_OF_TEST_RESULTS

vecho "Running test tests/sched-dst '0 * * * *' '1081062060'"
run_compare_test tests/sched-dst '0 * * * *' '1081062060' <<END_OF_TEST_RESULTS
last: 1081062060 2004-04-04 03:01:00 EDT
next: 1081065600 2004-04-04 04:00:00 EDT
END_OF_TEST_RESULTS

vecho "Running test tests/sched-dst '0 * * * *' '1099195260'"
run_compare_test tests/sched-dst '0 * * * *' '1099195260' <<END_OF_TEST_RESULTS
last: 1099195260 2004-10-31 00:01:00 EDT
next: 1099198800 2004-10-31 01:00:00 EDT
END_OF_TEST_RESULTS

vecho "Running test tests/sched-dst '0 * * * *' '1099198860'"
run_compare_test tests/sched-dst '0 * * * *' '1099198860' <<END_OF_TEST_RESULTS
last: 1099198860 2004-10-31 01:01:00 EDT
next: 1099202400 2004-10-31 01:00:00 EST
END_OF_TEST_RESULTS

vecho "Running test tests/sched-dst '0 * * * *' '1099202460'"
run_compare_test tests/sched-dst '0 * * * *' '1099202460' <<END_OF_TEST_RESULTS
last: 1099202460 2004-10-31 01:01:00 EST
next: 1099206000 2004-10-31 02:00:00 EST
END_OF_TEST_RESULTS

vecho "Running test tests/sched-dst '0 * * * *' '1099206060'"
run_compare_test tests/sched-dst '0 * * * *' '1099206060' <<END_OF_TEST_RESULTS
last: 1099206060 2004-10-31 02:01:00 EST
next: 1099209600 2004-10-31 03:00:00 EST
END_OF_TEST_RESULTS

vecho "Running test tests/sched-dst '0 0 * * *' '1081054860'"
run_compare_test tests/sched-dst '0 0 * * *' '1081054860' <<END_OF_TEST_RESULTS
last: 1081054860 2004-04-04 00:01:00 EST
next: 1081137600 2004-04-05 00:00:00 EDT
END_OF_TEST_RESULTS

vecho "Running test tests/sched-dst '0 0 * * *' '1081058460'"
run_compare_test tests/sched-dst '0 0 * * *' '1081058460' <<END_OF_TEST_RESULTS
last: 1081058460 2004-04-04 01:01:00 EST
next: 1081137600 2004-04-05 00:00:00 EDT
END_OF_TEST_RESULTS

vecho "Running test tests/sched-dst '0 0 * * *' '1081062060'"
run_compare_test tests/sched-dst '0 0 * * *' '1081062060' <<END_OF_TEST_RESULTS
last: 1081062060 2004-04-04 03:01:00 EDT
next: 1081137600 2004-04-05 00:00:00 EDT
END_OF_TEST_RESULTS

vecho "Running test tests/sched-dst '0 0 * * *' '1099195260'"
run_compare_test tests/sched-dst '0 0 * * *' '1099195260' <<END_OF_TEST_RESULTS
last: 1099195260 2004-10-31 00:01:00 EDT
next: 1099285200 2004-11-01 00:00:00 EST
END_OF_TEST_RESULTS

vecho "Running test tests/sched-dst '0 0 * * *' '1099198860'"
run_compare_test tests/sched-dst '0 0 * * *' '1099198860' <<END_OF_TEST_RESULTS
last: 1099198860 2004-10-31 01:01:00 EDT
next: 1099285200 2004-11-01 00:00:00 EST
END_OF_TEST_RESULTS

vecho "Running test tests/sched-dst '0 0 * * *' '1099202460'"
run_compare_test tests/sched-dst '0 0 * * *' '1099202460' <<END_OF_TEST_RESULTS
last: 1099202460 2004-10-31 01:01:00 EST
next: 1099285200 2004-11-01 00:00:00 EST
END_OF_TEST_RESULTS

vecho "Running test tests/sched-dst '0 0 * * *' '1099206060'"
run_compare_test tests/sched-dst '0 0 * * *' '1099206060' <<END_OF_TEST_RESULTS
last: 1099206060 2004-10-31 02:01:00 EST
next: 1099285200 2004-11-01 00:00:00 EST
END_OF_TEST_RESULTS

vecho "Running test tests/sched-dst '0 1 * * *' '1081054860'"
run_compare_test tests/sched-dst '0 1 * * *' '1081054860' <<END_OF_TEST_RESULTS
last: 1081054860 2004-04-04 00:01:00 EST
next: 1081058400 2004-04-04 01:00:00 EST
END_OF_TEST_RESULTS

vecho "Running test tests/sched-dst '0 1 * * *' '1081058460'"
run_compare_test tests/sched-dst '0 1 * * *' '1081058460' <<END_OF_TEST_RESULTS
last: 1081058460 2004-04-04 01:01:00 EST
next: 1081141200 2004-04-05 01:00:00 EDT
END_OF_TEST_RESULTS

vecho "Running test tests/sched-dst '0 1 * * *' '1081062060'"
run_compare_test tests/sched-dst '0 1 * * *' '1081062060' <<END_OF_TEST_RESULTS
last: 1081062060 2004-04-04 03:01:00 EDT
next: 1081141200 2004-04-05 01:00:00 EDT
END_OF_TEST_RESULTS

vecho "Running test tests/sched-dst '0 1 * * *' '1099195260'"
run_compare_test tests/sched-dst '0 1 * * *' '1099195260' <<END_OF_TEST_RESULTS
last: 1099195260 2004-10-31 00:01:00 EDT
next: 1099198800 2004-10-31 01:00:00 EDT
END_OF_TEST_RESULTS

vecho "Running test tests/sched-dst '0 1 * * *' '1099198860'"
run_compare_test tests/sched-dst '0 1 * * *' '1099198860' <<END_OF_TEST_RESULTS
last: 1099198860 2004-10-31 01:01:00 EDT
next: 1099288800 2004-11-01 01:00:00 EST
END_OF_TEST_RESULTS

vecho "Running test tests/sched-dst '0 1 * * *' '1099202460'"
run_compare_test tests/sched-dst '0 1 * * *' '1099202460' <<END_OF_TEST_RESULTS
last: 1099202460 2004-10-31 01:01:00 EST
next: 1099288800 2004-11-01 01:00:00 EST
END_OF_TEST_RESULTS

vecho "Running test tests/sched-dst '0 1 * * *' '1099206060'"
run_compare_test tests/sched-dst '0 1 * * *' '1099206060' <<END_OF_TEST_RESULTS
last: 1099206060 2004-10-31 02:01:00 EST
next: 1099288800 2004-11-01 01:00:00 EST
END_OF_TEST_RESULTS

vecho "Running test tests/sched-dst '0 2 * * *' '1081054860'"
run_compare_test tests/sched-dst '0 2 * * *' '1081054860' <<END_OF_TEST_RESULTS
last: 1081054860 2004-04-04 00:01:00 EST
next: 1081062000 2004-04-04 03:00:00 EDT
END_OF_TEST_RESULTS

vecho "Running test tests/sched-dst '0 2 * * *' '1081058460'"
run_compare_test tests/sched-dst '0 2 * * *' '1081058460' <<END_OF_TEST_RESULTS
last: 1081058460 2004-04-04 01:01:00 EST
next: 1081062000 2004-04-04 03:00:00 EDT
END_OF_TEST_RESULTS

vecho "Running test tests/sched-dst '0 2 * * *' '1081062060'"
run_compare_test tests/sched-dst '0 2 * * *' '1081062060' <<END_OF_TEST_RESULTS
last: 1081062060 2004-04-04 03:01:00 EDT
next: 1081144800 2004-04-05 02:00:00 EDT
END_OF_TEST_RESULTS

vecho "Running test tests/sched-dst '0 2 * * *' '1099195260'"
run_compare_test tests/sched-dst '0 2 * * *' '1099195260' <<END_OF_TEST_RESULTS
last: 1099195260 2004-10-31 00:01:00 EDT
next: 1099206000 2004-10-31 02:00:00 EST
END_OF_TEST_RESULTS

vecho "Running test tests/sched-dst '0 2 * * *' '1099198860'"
run_compare_test tests/sched-dst '0 2 * * *' '1099198860' <<END_OF_TEST_RESULTS
last: 1099198860 2004-10-31 01:01:00 EDT
next: 1099206000 2004-10-31 02:00:00 EST
END_OF_TEST_RESULTS

vecho "Running test tests/sched-dst '0 2 * * *' '1099202460'"
run_compare_test tests/sched-dst '0 2 * * *' '1099202460' <<END_OF_TEST_RESULTS
last: 1099202460 2004-10-31 01:01:00 EST
next: 1099206000 2004-10-31 02:00:00 EST
END_OF_TEST_RESULTS

vecho "Running test tests/sched-dst '0 2 * * *' '1099206060'"
run_compare_test tests/sched-dst '0 2 * * *' '1099206060' <<END_OF_TEST_RESULTS
last: 1099206060 2004-10-31 02:01:00 EST
next: 1099292400 2004-11-01 02:00:00 EST
END_OF_TEST_RESULTS

vecho "Running test tests/sched-dst '0 3 * * *' '1081054860'"
run_compare_test tests/sched-dst '0 3 * * *' '1081054860' <<END_OF_TEST_RESULTS
last: 1081054860 2004-04-04 00:01:00 EST
next: 1081062000 2004-04-04 03:00:00 EDT
END_OF_TEST_RESULTS

vecho "Running test tests/sched-dst '0 3 * * *' '1081058460'"
run_compare_test tests/sched-dst '0 3 * * *' '1081058460' <<END_OF_TEST_RESULTS
last: 1081058460 2004-04-04 01:01:00 EST
next: 1081062000 2004-04-04 03:00:00 EDT
END_OF_TEST_RESULTS

vecho "Running test tests/sched-dst '0 3 * * *' '1081062060'"
run_compare_test tests/sched-dst '0 3 * * *' '1081062060' <<END_OF_TEST_RESULTS
last: 1081062060 2004-04-04 03:01:00 EDT
next: 1081148400 2004-04-05 03:00:00 EDT
END_OF_TEST_RESULTS

vecho "Running test tests/sched-dst '0 3 * * *' '1099195260'"
run_compare_test tests/sched-dst '0 3 * * *' '1099195260' <<END_OF_TEST_RESULTS
last: 1099195260 2004-10-31 00:01:00 EDT
next: 1099209600 2004-10-31 03:00:00 EST
END_OF_TEST_RESULTS

vecho "Running test tests/sched-dst '0 3 * * *' '1099198860'"
run_compare_test tests/sched-dst '0 3 * * *' '1099198860' <<END_OF_TEST_RESULTS
last: 1099198860 2004-10-31 01:01:00 EDT
next: 1099209600 2004-10-31 03:00:00 EST
END_OF_TEST_RESULTS

vecho "Running test tests/sched-dst '0 3 * * *' '1099202460'"
run_compare_test tests/sched-dst '0 3 * * *' '1099202460' <<END_OF_TEST_RESULTS
last: 1099202460 2004-10-31 01:01:00 EST
next: 1099209600 2004-10-31 03:00:00 EST
END_OF_TEST_RESULTS

vecho "Running test tests/sched-dst '0 3 * * *' '1099206060'"
run_compare_test tests/sched-dst '0 3 * * *' '1099206060' <<END_OF_TEST_RESULTS
last: 1099206060 2004-10-31 02:01:00 EST
next: 1099209600 2004-10-31 03:00:00 EST
END_OF_TEST_RESULTS

vecho "Running test tests/sched-dst '0 0 1 * *' '1081054860'"
run_compare_test tests/sched-dst '0 0 1 * *' '1081054860' <<END_OF_TEST_RESULTS
last: 1081054860 2004-04-04 00:01:00 EST
next: 1083384000 2004-05-01 00:00:00 EDT
END_OF_TEST_RESULTS

vecho "Running test tests/sched-dst '0 0 1 * *' '1081058460'"
run_compare_test tests/sched-dst '0 0 1 * *' '1081058460' <<END_OF_TEST_RESULTS
last: 1081058460 2004-04-04 01:01:00 EST
next: 1083384000 2004-05-01 00:00:00 EDT
END_OF_TEST_RESULTS

vecho "Running test tests/sched-dst '0 0 1 * *' '1081062060'"
run_compare_test tests/sched-dst '0 0 1 * *' '1081062060' <<END_OF_TEST_RESULTS
last: 1081062060 2004-04-04 03:01:00 EDT
next: 1083384000 2004-05-01 00:00:00 EDT
END_OF_TEST_RESULTS

vecho "Running test tests/sched-dst '0 0 1 * *' '1099195260'"
run_compare_test tests/sched-dst '0 0 1 * *' '1099195260' <<END_OF_TEST_RESULTS
last: 1099195260 2004-10-31 00:01:00 EDT
next: 1099285200 2004-11-01 00:00:00 EST
END_OF_TEST_RESULTS

vecho "Running test tests/sched-dst '0 0 1 * *' '1099198860'"
run_compare_test tests/sched-dst '0 0 1 * *' '1099198860' <<END_OF_TEST_RESULTS
last: 1099198860 2004-10-31 01:01:00 EDT
next: 1099285200 2004-11-01 00:00:00 EST
END_OF_TEST_RESULTS

vecho "Running test tests/sched-dst '0 0 1 * *' '1099202460'"
run_compare_test tests/sched-dst '0 0 1 * *' '1099202460' <<END_OF_TEST_RESULTS
last: 1099202460 2004-10-31 01:01:00 EST
next: 1099285200 2004-11-01 00:00:00 EST
END_OF_TEST_RESULTS

vecho "Running test tests/sched-dst '0 0 1 * *' '1099206060'"
run_compare_test tests/sched-dst '0 0 1 * *' '1099206060' <<END_OF_TEST_RESULTS
last: 1099206060 2004-10-31 02:01:00 EST
next: 1099285200 2004-11-01 00:00:00 EST
END_OF_TEST_RESULTS


##### Test tests/spool-list-missing #####

runtest() {
rm -f $CRONTABS/$USER
printf "L$USER" | spoolcmd
}
vecho "Running test tests/spool-list-missing "
run_compare_test tests/spool-list-missing  <<END_OF_TEST_RESULTS
23:DCrontab does not exist,
END_OF_TEST_RESULTS


##### Test tests/bcrontab #####

runtest() {
startspool

bcrontab -l
echo $?

echo 'Test crontab' >$CRONTABS/$USER
bcrontab -l
echo $?
rm -f $CRONTABS/$USER

bcrontab -l -u nobody
echo $?

echo
stopspool
}
vecho "Running test tests/bcrontab "
run_compare_test tests/bcrontab  <<END_OF_TEST_RESULTS
bcrontab: Fatal: Crontab does not exist
111
Test crontab
0
bcrontab: Fatal: Username does not match invoking UID
111

bcron-spool[#]: L bruce
bcron-spool[#]: Fatal: bruce: Crontab does not exist
bcron-spool[#]: L bruce
bcron-spool[#]: Fatal: nobody: Username does not match invoking UID
END_OF_TEST_RESULTS


##### Test tests/spool-delete-good #####

runtest() {
touch $CRONTABS/$USER
printf "R$USER" | spoolcmd
test -e $CRONTABS/$USER && echo File still exists.
}
vecho "Running test tests/spool-delete-good "
run_compare_test tests/spool-delete-good  <<END_OF_TEST_RESULTS
16:KCrontab removed,
END_OF_TEST_RESULTS


##### Test tests/sched-misc #####

runtest() {
local job="$1"
local start="$2"
export TZ=CST
sched "$job echo" $start
unset TZ
}
vecho "Running test tests/sched-misc '*/10 * * * *' '1081058400'"
run_compare_test tests/sched-misc '*/10 * * * *' '1081058400' <<END_OF_TEST_RESULTS
last: 1081058400 2004-04-04 06:00:00 CST
next: 1081058400 2004-04-04 06:00:00 CST
END_OF_TEST_RESULTS

vecho "Running test tests/sched-misc '*/10 * * * *' '1081058460'"
run_compare_test tests/sched-misc '*/10 * * * *' '1081058460' <<END_OF_TEST_RESULTS
last: 1081058460 2004-04-04 06:01:00 CST
next: 1081059000 2004-04-04 06:10:00 CST
END_OF_TEST_RESULTS

vecho "Running test tests/sched-misc '1 1 1 Jan *' '1081058400'"
run_compare_test tests/sched-misc '1 1 1 Jan *' '1081058400' <<END_OF_TEST_RESULTS
last: 1081058400 2004-04-04 06:00:00 CST
next: 1104541260 2005-01-01 01:01:00 CST
END_OF_TEST_RESULTS

vecho "Running test tests/sched-misc '1 1 1 Jan *' '1081058460'"
run_compare_test tests/sched-misc '1 1 1 Jan *' '1081058460' <<END_OF_TEST_RESULTS
last: 1081058460 2004-04-04 06:01:00 CST
next: 1104541260 2005-01-01 01:01:00 CST
END_OF_TEST_RESULTS

vecho "Running test tests/sched-misc '1 1 * * Wed' '1081058400'"
run_compare_test tests/sched-misc '1 1 * * Wed' '1081058400' <<END_OF_TEST_RESULTS
last: 1081058400 2004-04-04 06:00:00 CST
next: 1081299660 2004-04-07 01:01:00 CST
END_OF_TEST_RESULTS

vecho "Running test tests/sched-misc '1 1 * * Wed' '1081058460'"
run_compare_test tests/sched-misc '1 1 * * Wed' '1081058460' <<END_OF_TEST_RESULTS
last: 1081058460 2004-04-04 06:01:00 CST
next: 1081299660 2004-04-07 01:01:00 CST
END_OF_TEST_RESULTS

vecho "Running test tests/sched-misc '3 1 2 * Wed' '1081058400'"
run_compare_test tests/sched-misc '3 1 2 * Wed' '1081058400' <<END_OF_TEST_RESULTS
last: 1081058400 2004-04-04 06:00:00 CST
next: 1086138180 2004-06-02 01:03:00 CST
END_OF_TEST_RESULTS

vecho "Running test tests/sched-misc '3 1 2 * Wed' '1081058460'"
run_compare_test tests/sched-misc '3 1 2 * Wed' '1081058460' <<END_OF_TEST_RESULTS
last: 1081058460 2004-04-04 06:01:00 CST
next: 1086138180 2004-06-02 01:03:00 CST
END_OF_TEST_RESULTS


##### Test tests/spool-submit-nouser #####

runtest() {
printf "Sxyzzy$USER\\0* * * * * echo hello\\n" | spoolcmd
rm -f $CRONTABS/xyzzy$USER
}
vecho "Running test tests/spool-submit-nouser "
run_compare_test tests/spool-submit-nouser  <<END_OF_TEST_RESULTS
28:DInvalid or unknown username,
END_OF_TEST_RESULTS


##### Test tests/exec-fds #####

runtest() {
echo 'exec 2>/dev/null' >$tmp/echo-all.sh
fd=4
while [ $fd -lt 10 ]
do
  echo "echo here >&$fd || echo safe fd $fd" >>$tmp/echo-all.sh
  fd=$(( $fd + 1 ))
done
echo 'echo first done' >>$tmp/echo-all.sh

doexec \
	'sleep 1; echo all done' \
	"sh $tmp/echo-all.sh"

rm -f $tmp/echo-all.sh
}
vecho "Running test tests/exec-fds "
run_compare_test tests/exec-fds  <<END_OF_TEST_RESULTS
15:2^@KJob complete,15:1^@KJob complete,
bcron-exec: (USER) CMD (sleep 1; echo all done)
bcron-exec: (USER) CMD (sh $tmp/echo-all.sh)
bcron-exec: Waiting for remaining slots to complete
To: <USER>
From: Cron Daemon <root@HOST>
Subject: Cron <USER@HOST> sh $tmp/echo-all.sh

safe fd 4
safe fd 5
safe fd 6
safe fd 7
safe fd 8
safe fd 9
first done
To: <USER>
From: Cron Daemon <root@HOST>
Subject: Cron <USER@HOST> sleep 1; echo all done

all done
END_OF_TEST_RESULTS


##### Test tests/sched-dump #####

runtest() {
cat >$CRONTABS/:etc:crontab <<EOF
* * * * * root line1
0-10/3 * * * * nobody line2
30 1 2 3 4 nobody line3
EOF

startsched
kill -USR1 $sched_pid
stopsched
}
vecho "Running test tests/sched-dump "
run_compare_test tests/sched-dump  <<END_OF_TEST_RESULTS
bcron-sched: Starting
bcron-sched: Loading ':etc:crontab'
bcron-sched: Crontab ":etc:crontab":
bcron-sched: M:0-59 H:0-23 d:1-31 m:0-11 wd:0-6 hc:24 next:####-##-## ##:##:## #TZ runas:(root) cmd:(line1)
bcron-sched: M:0,3,6,9 H:0-23 d:1-31 m:0-11 wd:0-6 hc:24 next:####-##-## ##:##:## #TZ runas:(nobody) cmd:(line2)
bcron-sched: M:30 H:1 d:2 m:2 wd:4 hc:1 next:####-##-## ##:##:## #TZ runas:(nobody) cmd:(line3)
END_OF_TEST_RESULTS


##### Test tests/spool-submit-baduser #####

runtest() {
printf "Sroot\\0* * * * * echo hello\\n" | spoolcmd
rm -f $CRONTABS/root
}
vecho "Running test tests/spool-submit-baduser "
run_compare_test tests/spool-submit-baduser  <<END_OF_TEST_RESULTS
37:DUsername does not match invoking UID,
END_OF_TEST_RESULTS


##### Test tests/exec-simple #####

runtest() {
doexec 'echo yes'
}
vecho "Running test tests/exec-simple "
run_compare_test tests/exec-simple  <<END_OF_TEST_RESULTS
15:1^@KJob complete,
bcron-exec: (USER) CMD (echo yes)
bcron-exec: Waiting for remaining slots to complete
To: <USER>
From: Cron Daemon <root@HOST>
Subject: Cron <USER@HOST> echo yes

yes
END_OF_TEST_RESULTS


##### Test tests/spool-submit #####

runtest() {
printf "S$USER\\0* * * * * echo hello\\n" | spoolcmd
cat $CRONTABS/$USER
rm -f $CRONTABS/$USER
}
vecho "Running test tests/spool-submit "
run_compare_test tests/spool-submit  <<END_OF_TEST_RESULTS
29:KCrontab successfully written,
* * * * * echo hello
END_OF_TEST_RESULTS


##### Test tests/spool-filter #####

runtest() {
printf "S$USER\\0* * * * * echo hello\\n" | spoolcmd wc
cat $CRONTABS/$USER
rm -f $CRONTABS/$USER
}
vecho "Running test tests/spool-filter "
run_compare_test tests/spool-filter  <<END_OF_TEST_RESULTS
29:KCrontab successfully written,
 1  7 21
END_OF_TEST_RESULTS


##### Test tests/spool-delete-baduser #####

runtest() {
touch $CRONTABS/root
printf "Rroot" | spoolcmd
test -e $CRONTABS/root && echo File still exists.
}
vecho "Running test tests/spool-delete-baduser "
run_compare_test tests/spool-delete-baduser  <<END_OF_TEST_RESULTS
37:DUsername does not match invoking UID,
File still exists.
END_OF_TEST_RESULTS


rm -rf $tmp
echo $tests_count tests executed, $tests_failed failures
if [ $tests_failed != 0 ]; then exit 1; fi
